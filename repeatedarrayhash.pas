unit RepeatedArrayHash;

{$mode objfpc}{$H+}{$J-}

interface

uses
  Classes, SysUtils;

type
  PRepeated = ^TRepeated;
  TRepeated = record
    Next: PRepeated;
    Taken: Boolean;
    Repeated: Boolean;
    Value: Integer;
  end;

  TRepeatedArrayHash = array of TRepeated;

  TRepeatedElements = array of Integer;

  function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;

implementation

function HashValue(Value: Integer): LongWord;
begin
  Result := Value * 2654435761;
end;

procedure InsertRepeatedElement(var RepeatedElements: TRepeatedElements; NewElement: Integer);
var
  CurrentSize: SizeUInt;
begin
  CurrentSize := Length(RepeatedElements);
  SetLength(RepeatedElements, CurrentSize + 1);
  RepeatedElements[CurrentSize] := NewElement;
end;

function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;
var
  ArrayHash: TRepeatedArrayHash;
  ArrayHashSize: SizeUInt;
  i: SizeUInt;
  Hash: SizeUInt;
  Current, Next: PRepeated;
begin
  ArrayHashSize := Length(Elements) div 2;
  SetLength(ArrayHash, ArrayHashSize);

  for i := Low(Elements) to High(Elements) do
  begin
    Hash := HashValue(Elements[i]) mod ArrayHashSize;
    if not ArrayHash[Hash].Taken then
    begin
      ArrayHash[Hash].Value := Elements[i];
      ArrayHash[Hash].Taken := True;
      ArrayHash[Hash].Next := Nil;
      ArrayHash[Hash].Repeated := False;
      Continue;
    end;

    if ArrayHash[Hash].Value = Elements[i] then
    begin
      ArrayHash[Hash].Repeated := True;
      Continue;
    end;

    if ArrayHash[Hash].Next = Nil then
    begin
      New(ArrayHash[Hash].Next);
      ArrayHash[Hash].Next^.Value := Elements[i];
      ArrayHash[Hash].Next^.Taken := True;
      ArrayHash[Hash].Next^.Repeated := False;
      ArrayHash[Hash].Next^.Next := Nil;
      Continue;
    end;

    Next := ArrayHash[Hash].Next;
    while Next <> Nil do
    begin
      if Next^.Value = Elements[i] then
      begin
        Next^.Repeated := True;
        Next := Next^.Next;
        Continue;
      end;

      if Next^.Next = Nil then
      begin
        New(Next^.Next);
        Next^.Next^.Value := Elements[i];
        Next^.Next^.Repeated := False;
        Next^.Next^.Taken := True;
        Next^.Next^.Next := Nil;
        Next := Next^.Next^.Next;
        Continue;
      end;
    end;

  end;

  SetLength(Result, 0);

  for i := Low(ArrayHash) to High(ArrayHash) do
  begin
    if not ArrayHash[i].Taken then
      Continue;

    if ArrayHash[i].Repeated then
    begin
      InsertRepeatedElement(Result, ArrayHash[i].Value);
    end;

    if ArrayHash[i].Next <> Nil then
    begin
      Next := ArrayHash[i].Next;
      while Next <> Nil do
      begin
        if Next^.Repeated then
        begin
          InsertRepeatedElement(Result, Next^.Value);
        end;
        Current := Next;
        Next := Next^.Next;
        Dispose(Current);
      end;
    end;
  end;

end;

end.

