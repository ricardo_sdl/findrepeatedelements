unit RepeatedTree;

{$mode objfpc}{$H+}{$J-}

interface

uses
  Classes, SysUtils;

type
  PRepeatedNode = ^TRepeatedNode;
  PPRepeatedNode = ^PRepeatedNode;
  TRepeatedNode = record
    Left: PRepeatedNode;
    Right: PRepeatedNode;
    Value: Integer;
  end;

  TRepeatedTree = record
    Left: PRepeatedNode;
    Right: PRepeatedNode;
  end;
  PRepeatedTree = ^TRepeatedTree;

  TRepeatedElements = array of Integer;

  function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;

  procedure PrintRepeatedValuesTree(Root: PRepeatedNode);

implementation

function CreateRepeatedNode(Value: Integer): PRepeatedNode;
begin
  New(Result);
  if Result = Nil then
    Exit;
  Result^.Value := Value;
  Result^.Left := Nil;
  Result^.Right := Nil;
end;

function InsertRepeatedNode(Root: PPRepeatedNode; Value: Integer; var NumRepeatedElements: SizeUInt): Boolean;
begin
  if Root^ = Nil then
  begin
    Root^ := CreateRepeatedNode(Value);
    if Root^ = Nil then Result := False;
  end;

  if Root^^.Value = Value then
  begin
    if Root^^.Left <> Nil then
      Exit(True);
    Root^^.Left := CreateRepeatedNode(Value);
    if Root^^.Left = Nil then Exit(False);
    Inc(NumRepeatedElements);
    Exit(True);
  end;

  if Root^^.Right = Nil then
  begin
    Root^^.Right := CreateRepeatedNode(Value);
    if Root^^.Right = Nil then Exit(False);
    Exit(True);
  end;

  Exit(InsertRepeatedNode(@Root^^.Right, Value, NumRepeatedElements));

end;

function CreateRepeatedValuesTree(Elements: array of Integer; var NumRepeatedElements: SizeUInt): PRepeatedNode;
var
  i: LongWord;

begin
  NumRepeatedElements := 0;
  if Length(Elements) < 2 then
    Exit(Nil);

  Result := CreateRepeatedNode(Elements[Low(Elements)]);

  for i := Low(Elements) + 1 to High(Elements) do
  begin
    InsertRepeatedNode(@Result, Elements[i], NumRepeatedElements);
  end;


end;

function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;
var
  Root: PRepeatedNode;
  i: SizeUInt = 0;
  NumRepeatedElements: SizeUInt;
begin
  Root := CreateRepeatedValuesTree(Elements, NumRepeatedElements);
  SetLength(Result, NumRepeatedElements);
  while Root <> Nil do
  begin
    if Root^.Left <> Nil then
    begin
      Result[i] := Root^.Value;
      Inc(i);
    end;
    Root := Root^.Right;
  end;
end;

procedure PrintRepeatedValuesTree(Root: PRepeatedNode);
begin

  if Root = Nil then
    Exit;

  if Root^.Left = Nil then
    WriteLn('Non repeated:', Root^.Value)
  else
    WriteLn('Repeated:', Root^.Value);

  if Root^.Right <> Nil then
    PrintRepeatedValuesTree(Root^.Right);


end;

end.

