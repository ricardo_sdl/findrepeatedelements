{$mode objfpc}{$H+}{$J-}
program FindRepeatedElements;

uses Classes, RepeatedTree, RepeatedArray, sysutils, dateutils, RepeatedArrayHash;

const
  MaxElements = 1000000;

procedure PrintElements(Elements: array of Integer);
var
  i: LongWord;
begin

  WriteLn();

  for i := 0 to Length(Elements) - 1 do
    Write(Elements[i], ' ');
  WriteLn();

end;

var
  Elements: array of Integer;
  ResultRepeatedTree: RepeatedTree.TRepeatedElements;
  ResultRepeatedArray: RepeatedArray.TRepeatedElements;
  ResultRepeatedArrayHash: RepeatedArrayHash.TRepeatedElements;
  i: Integer;
  Start, Ending: TDateTime;

begin

  Randomize;

  SetLength(Elements, MaxElements);
  for i := 0 to MaxElements - 1 do
    Elements[i] := Random(Trunc(0.45 * MaxElements));

  //Start := Now();
  //ResultRepeatedArray := RepeatedArray.GetRepeatedElements(Elements);
  //Ending := Now();
  //
  //WriteLn('RepeatedArray took ', MilliSecondsBetween(Ending, Start), ' ms');
  //PrintElements(ResultRepeatedArray);

  Start := Now();
  ResultRepeatedArrayHash := RepeatedArrayHash.GetRepeatedElements(Elements);
  Ending := Now();
  WriteLn('RepeatedArrayHash took ', MilliSecondsBetween(Ending, Start), ' ms');
  WriteLn('Found ', Length(ResultRepeatedArrayHash), ' elements');
  //PrintElements(ResultRepeatedArrayHash);

  //ReadLn;

end.

