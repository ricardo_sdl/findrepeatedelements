unit RepeatedArray;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ghashset;

type

  { THashAlgo }

  THashAlgo = class
    public
      class function Hash(a: Integer; b: LongWord): LongWord;
  end;

  TRepeatedElements = array of Integer;
  THashSetRepeatedElements = specialize THashSet<Integer, THashAlgo>;

  function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;

implementation

function GetRepeatedElements(Elements: array of Integer): TRepeatedElements;
var
  CurrentElement, ComparedElement: LongWord;
  HashRepeatedElements: THashSetRepeatedElements;
  HashIterator: THashSetRepeatedElements.TIterator;
  i: LongWord = 0;
begin
  SetLength(Result, 0);
  if Length(Elements) < 2 then
    Exit;

  if Length(Elements) = 2 then
  begin
    if Elements[0] = Elements[1] then
    begin
      SetLength(Result, 1);
      Result[0] := Elements[0];
    end;
    Exit;
  end;

  HashRepeatedElements := THashSetRepeatedElements.create;

  for CurrentElement := 0 to Length(Elements) - 2 do
  begin
    for ComparedElement := CurrentElement + 1 to Length(Elements) - 1 do
    begin
      if Elements[CurrentElement] = Elements[ComparedElement] then
      begin
        HashRepeatedElements.insert(Elements[CurrentElement]);
        Break;
      end;
    end;
  end;

  HashIterator := HashRepeatedElements.Iterator();
  SetLength(Result, HashRepeatedElements.size());

  repeat
    Result[i] := HashIterator.Data;
    Inc(i);
  until not HashIterator.Next;

  FreeAndNil(HashIterator);

  FreeAndNil(HashRepeatedElements);

end;

{ THashAlgo }

class function THashAlgo.Hash(a: Integer; b: LongWord): LongWord;
begin
  Result := a mod b;
end;

end.

